//
//  ViewController.swift
//  AttachmentController
//
//  Created by Woessner, Philipp on 13.12.19.
//  Copyright © 2019 sap. All rights reserved.
//

import UIKit

import SAPFiori
import Photos
import WebKit

class AttachmentAction: FUIAttachmentAction {
    var title: String
    var vc: ViewController!
    
    init(vc: ViewController, title: String) {
        self.vc = vc
        self.title = title
    }
    
    func action(onController controller: FUIAttachmentsViewController) {
        vc.addAttachment(URL(string: "https://google.com/maps")!)
    }
    
    func isAvailable(_ alertAction: UIAlertAction) -> Bool {
        return true
    }
}

class ViewController: FUIFormTableViewController {
    
    var attachmentURLs = [URL]()
    var attachmentThumbnails = [String: UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Register the types of cells to be used
        tableView.register(FUIAttachmentsFormCell.self, forCellReuseIdentifier: FUIAttachmentsFormCell.reuseIdentifier)

        tableView.estimatedRowHeight = 180
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
    }
    
    func addAttachment(_ url: URL) {
        attachmentURLs.append(url)
        tableView.reloadData()
    }

    // UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 // return number of rows of data source
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let attachmentCell = tableView.dequeueReusableCell(withIdentifier: FUIAttachmentsFormCell.reuseIdentifier, for: indexPath) as! FUIAttachmentsFormCell
        
        attachmentCell.attachmentsController.delegate = self
        attachmentCell.attachmentsController.dataSource = self
        attachmentCell.attachmentsController.reloadData()
        
        attachmentCell.attachmentsController.customAttachmentsTitleFormat = attachmentURLs.count > 1 ? "Attachments (\(attachmentURLs.count))" : "Attachment (\(attachmentURLs.count))"

        let attachmentAction = AttachmentAction(vc: self, title: "Add Attachment")
        attachmentCell.attachmentsController.addAttachmentAction(attachmentAction)

        return attachmentCell
    }


    // This function is invoked for setup thumbnails
    private func setupThumbnails(_ attaURL: URL, with asset: PHAsset) {
        let imageManager = PHImageManager.default()
        imageManager.requestImage(for: asset, targetSize: CGSize(width: 80, height: 80), contentMode: .default,
        options: nil, resultHandler: { image, _ in
            self.attachmentThumbnails[attaURL.absoluteString] = image
            self.tableView.reloadData()

        })
    }
}

// MARK: FUIAttachmentsViewControllerDataSource
extension ViewController: FUIAttachmentsViewControllerDataSource {

    // Gets the number of attachments to display in the FUIAttachmentsViewController's collection
    func numberOfAttachments(in attachmentsViewController: FUIAttachmentsViewController) -> Int {
        return attachmentURLs.count
    }

    // Gets the file URL of the attachment resource
    func attachmentsViewController(_ attachmentsViewController: FUIAttachmentsViewController,
         urlForAttachmentAtIndex index: Int) -> URL? {
        var modifiedUrl = attachmentURLs[index]
        modifiedUrl.deleteLastPathComponent()
        modifiedUrl.appendPathComponent("File \(index).pdf")
        return modifiedUrl
    }

    // Gets the thumbnail image for the attachment at the specified index
    func attachmentsViewController(_ attachmentsViewController: FUIAttachmentsViewController,
         iconForAttachmentAtIndex index: Int) -> (image: UIImage, contentMode: UIView.ContentMode)? {
        return nil
    }
    
    func attachmentsViewController(_ attachmentsViewController: FUIAttachmentsViewController, didSelectAtIndex index: Int) -> Bool {
        // open attachmentUrl in WebView
        // something like this
        // make sure to use the real url here!
        let webView = WKWebView()
        webView.load(URLRequest(url: attachmentURLs[index]))
        let controller = UIViewController()
        controller.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: controller.view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: controller.view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: controller.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: controller.view.trailingAnchor).isActive = true
        self.present(controller,animated: true)
        return false
    }
}

// MARK: FUIAttachmentsViewControllerDelegate
extension ViewController: FUIAttachmentsViewControllerDelegate {

    // To notify when user tapped delete to an attachment
    func attachmentsViewController(_ attachmentsViewController: FUIAttachmentsViewController,
         didPressDeleteAtIndex index: Int) {
        attachmentURLs.remove(at: index)
        tableView.reloadData()
    }

    // This function is invoked, if the controller failed to obtain a valid file URL for an attachment which should be presented
    func attachmentsViewController(_ attachmentsViewController: FUIAttachmentsViewController,
         couldNotPresentAttachmentAtIndex index: Int) {
        // handle attachments which cannot be presented
    }
}


